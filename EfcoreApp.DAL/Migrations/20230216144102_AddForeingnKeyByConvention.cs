﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EfcoreApp.DAL.Migrations
{
    public partial class AddForeingnKeyByConvention : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TaskOwnerId",
                table: "Tasks");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TaskOwnerId",
                table: "Tasks",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
