﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace EfcoreApp.DAL
{
    public class ToDoAppDbContextFactory : IDesignTimeDbContextFactory<ToDoDbContext>
    {

        public ToDoAppDbContextFactory()
        {
            
        }

        public ToDoDbContext CreateDbContext(string[] args)
        {
            var OptionBuilder = new DbContextOptionsBuilder<ToDoDbContext>();
            var ConnectionString = @"Data Source=DESKTOP-EQTI4R6\SQLEXPRESS;Initial Catalog=ToDoListDb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            OptionBuilder.UseSqlServer(ConnectionString);
            return new ToDoDbContext(OptionBuilder.Options);
        }
    }
}
