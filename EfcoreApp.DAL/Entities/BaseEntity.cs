﻿using System;

namespace EfcoreApp.DAL.Entities;

public class BaseEntity
{
    public int Id { get; set; }

    public DateTime Created { get; set; }
    public DateTime Updated { get; set; }
    public byte[] TimeStamp { get; set; }
}